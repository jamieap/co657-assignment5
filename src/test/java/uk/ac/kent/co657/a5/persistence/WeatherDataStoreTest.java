package uk.ac.kent.co657.a5.persistence;

import com.google.common.base.Throwables;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.junit.Before;
import org.junit.Test;
import uk.ac.kent.co657.a5.persistence.model.DataType;
import uk.ac.kent.co657.a5.persistence.model.WeatherRecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.jooq.impl.DSL.table;
import static org.junit.Assert.*;

public class WeatherDataStoreTest {

    private WeatherDataStore db;

    @Before
    public void setUp() throws Exception {
        DSLContext dsl = DSL.using(getDbConnection());
        db = new WeatherDataStore(dsl, "weather_data");
        dsl.truncate(table("weather_data")).execute();
    }

    @Test
    public void testSaveData() throws Exception {
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.HOURS);
        for (int i = 0; i < 48; i++) {
            db.saveData(now.minus(i, ChronoUnit.MONTHS), DataType.TEMPERATURE, 10f, "test");
        }
    }

    @Test
    public void testGetData() throws Exception {
        List<WeatherRecord> data = db.getData(DataType.TEMPERATURE, Granularity.MONTH, 48);
        assertNotNull(data);
        assertEquals(data.size(), 48);
    }

    public Connection getDbConnection() {
        try {
            return DriverManager.getConnection("jdbc:mysql://localhost:3306/weather_data_test", "root", "");
        } catch (SQLException e) {
            throw Throwables.propagate(e);
        }
    }
}