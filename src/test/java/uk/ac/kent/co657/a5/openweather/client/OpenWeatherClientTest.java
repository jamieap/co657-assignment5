package uk.ac.kent.co657.a5.openweather.client;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.common.util.concurrent.ListenableFuture;
import uk.ac.kent.co657.a5.openweather.model.SimpleWeatherData;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class OpenWeatherClientTest {

    @Test
    public void testGetWeatherData() throws Exception {
        OpenWeatherClient client = new OpenWeatherClient(
                new NetHttpTransport(), "dae83332aa9b6c37dd4358db56a97acd"
        );
        SimpleWeatherData data = client.getWeatherData("canterbury,uk").get();
        assertNotNull(data.getTemperature());
        assertNotNull(data.getHumidity());
        assertNotNull(data.getPressure());
    }
}