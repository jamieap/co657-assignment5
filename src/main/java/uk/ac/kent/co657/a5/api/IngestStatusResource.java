package uk.ac.kent.co657.a5.api;

import com.google.common.collect.Maps;
import uk.ac.kent.co657.a5.persistence.WeatherDataStore;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Resource
@Path("/ingest/status")
public class IngestStatusResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, String> getLastUpdateTimes(@Context WeatherDataStore db) {
        Map<String, LocalDateTime> lastUpdateBySources = db.getSourceInfo();
        return Maps.transformValues(
                lastUpdateBySources,
                val -> val.format(DateTimeFormatter.ISO_DATE_TIME)
        );
    }
}
