package uk.ac.kent.co657.a5;

import com.codahale.metrics.health.HealthCheck;
import uk.ac.kent.co657.a5.openweather.OpenWeatherDataFetcher;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static com.google.common.base.Preconditions.checkNotNull;

public class OpenWeatherHealthCheck extends HealthCheck {

    public static final String MESSAGE = "OpenWeather fetcher last ran at %s";
    private final OpenWeatherDataFetcher fetcher;

    public OpenWeatherHealthCheck(OpenWeatherDataFetcher fetcher) {
        this.fetcher = checkNotNull(fetcher);
    }

    @Override
    protected Result check() throws Exception {
        LocalDateTime lastFetchTime = fetcher.getLastFetchTime();
        if (lastFetchTime == null) {
            return Result.unhealthy("OpenWeather fetcher has not run");
        }
        if (lastFetchTime.isAfter(LocalDateTime.now().minus(1, ChronoUnit.HOURS))) {
            return Result.healthy(MESSAGE, lastFetchTime);
        }
        return Result.unhealthy(MESSAGE, lastFetchTime);
    }
}