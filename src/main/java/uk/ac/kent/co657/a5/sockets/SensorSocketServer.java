package uk.ac.kent.co657.a5.sockets;

import com.google.common.base.Throwables;
import io.dropwizard.lifecycle.Managed;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.socket.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A Netty socket server. It is used to receive JSON from the K64Fs over a TCP socket.
 */
public class SensorSocketServer implements Managed {

    private static final Logger LOG = LoggerFactory.getLogger(SensorSocketServer.class);

    private final NioEventLoopGroup acceptorGroup;
    private final NioEventLoopGroup handlerGroup;
    private final Integer port;
    private final PersistingSensorReadingHandler handler;

    public SensorSocketServer(
            NioEventLoopGroup acceptorGroup,
            NioEventLoopGroup handlerGroup,
            Integer port,
            PersistingSensorReadingHandler handler
    ) {
        this.acceptorGroup = checkNotNull(acceptorGroup);
        this.handlerGroup = checkNotNull(handlerGroup);
        this.port = checkNotNull(port);
        this.handler = checkNotNull(handler);
    }

    @Override
    public void start() {
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(acceptorGroup, handlerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new SensorSocketInitializer(handler))
                    .option(ChannelOption.SO_BACKLOG, 5)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            b.localAddress(port).bind().sync();
            LOG.info("Started on port {}", port);
        } catch (Exception e) {
            Throwables.propagate(e);
        }
    }

    @Override
    public void stop() {
        acceptorGroup.shutdown();
        handlerGroup.shutdown();
        LOG.info("Stopped");
    }

}