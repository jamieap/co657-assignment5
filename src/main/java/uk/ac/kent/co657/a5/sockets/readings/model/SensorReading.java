package uk.ac.kent.co657.a5.sockets.readings.model;

import com.google.common.base.MoreObjects;

public class SensorReading {

    private final BMP180 bmp180;
    private final DHT22 dht22;
    private final String name;

    public SensorReading(BMP180 bmp180, DHT22 dht22, String name) {
        this.bmp180 = bmp180;
        this.dht22 = dht22;
        this.name = name;
    }

    public BMP180 getBmp180() {
        return bmp180;
    }

    public DHT22 getDht22() {
        return dht22;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("bmp180", bmp180.toString())
                .add("dht22", dht22.toString())
                .add("name", name)
                .toString();
    }

}