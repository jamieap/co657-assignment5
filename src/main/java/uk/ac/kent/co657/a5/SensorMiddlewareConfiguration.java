package uk.ac.kent.co657.a5;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Throwables;
import io.dropwizard.Configuration;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.PropertyConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

public class SensorMiddlewareConfiguration extends Configuration {

    @JsonProperty("socketServer")
    private Map<String, Object> socketServer;

    @JsonProperty("db")
    private Map<String, Object> db;

    @JsonProperty("openWeather")
    private Map<String, Object> openWeather;

    @JsonProperty("twitter")
    private Map<String, Object> twitter;

    public String getOpenWeatherApiKey() {
        return (String) openWeather.get("apiKey");
    }

    public String getOpenWeatherLocation() {
        return (String) openWeather.get("location");
    }

    public Integer getSocketServerPort() {
        return (Integer) socketServer.get("port");
    }

    public Integer getSocketServerAcceptorThreads() {
        return (Integer) socketServer.get("acceptorThreads");
    }

    public Integer getSocketServerHandlerThreads() {
        return (Integer) socketServer.get("handleThreads");
    }

    private String getDbUrl() {
        return (String) db.get("url");
    }

    private String getDbUsername() {
        return (String) db.get("username");
    }

    private String getDbPassword() {
        return (String) db.get("password");
    }

    public String getDbName() {
        return (String) db.get("table");
    }

    public Twitter getTwitter() {
        Properties props = new Properties();
        props.setProperty("oauth.consumerKey", (String) twitter.get("consumerKey"));
        props.setProperty("oauth.consumerSecret", (String) twitter.get("consumerSecret"));
        AccessToken accessToken = new AccessToken(
                (String) twitter.get("accessToken"),
                (String) twitter.get("accessTokenSecret")
        );
        return new TwitterFactory(new PropertyConfiguration(props)).getInstance(accessToken);
    }

    public Connection getDbConnection() {
        try {
            return DriverManager.getConnection(getDbUrl(), getDbUsername(), getDbPassword());
        } catch (SQLException e) {
            throw Throwables.propagate(e);
        }
    }
}
