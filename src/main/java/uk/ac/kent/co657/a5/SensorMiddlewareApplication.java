package uk.ac.kent.co657.a5;

import com.google.api.client.http.javanet.NetHttpTransport;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import io.netty.channel.socket.nio.NioEventLoopGroup;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import uk.ac.kent.co657.a5.api.IngestStatusResource;
import uk.ac.kent.co657.a5.api.WeatherDataResource;
import uk.ac.kent.co657.a5.openweather.OpenWeatherDataFetcher;
import uk.ac.kent.co657.a5.openweather.client.OpenWeatherClient;
import uk.ac.kent.co657.a5.persistence.WeatherDataStore;
import uk.ac.kent.co657.a5.sockets.PersistingSensorReadingHandler;
import uk.ac.kent.co657.a5.sockets.SensorSocketServer;
import uk.ac.kent.co657.a5.twitter.DataTweeter;

import java.sql.Connection;

public class SensorMiddlewareApplication extends Application<SensorMiddlewareConfiguration> {

    public static void main(String[] args) throws Exception {
        new SensorMiddlewareApplication().run(args);
    }

    @Override
    public void run(SensorMiddlewareConfiguration cfg, Environment env) throws Exception {
        WeatherDataStore db = createWeatherDataStore(cfg);
        SensorSocketServer socketServer = createSensorSocketServer(cfg, db);
        OpenWeatherDataFetcher fetcher = createOpenWeatherFetcher(cfg, db);
        DataTweeter managed = new DataTweeter(cfg.getTwitter(), db);

        env.lifecycle().manage(managed);
        env.lifecycle().manage(socketServer);
        env.lifecycle().manage(fetcher);

        env.jersey().register(abstractBindings(db));
        env.jersey().register(WeatherDataResource.class);
        env.jersey().register(IngestStatusResource.class);
        env.jersey().register(CorsFilter.class);

        env.healthChecks().register("openweather-fetch", new OpenWeatherHealthCheck(fetcher));
    }

    private OpenWeatherDataFetcher createOpenWeatherFetcher(SensorMiddlewareConfiguration cfg, WeatherDataStore db) {
        NetHttpTransport httpTransport = new NetHttpTransport();
        OpenWeatherClient client = new OpenWeatherClient(httpTransport, cfg.getOpenWeatherApiKey());
        return new OpenWeatherDataFetcher(
                db,
                client,
                cfg.getOpenWeatherLocation()
        );
    }

    private AbstractBinder abstractBindings(final WeatherDataStore db) {
        return new AbstractBinder() {
            @Override
            protected void configure() {
                bind(db).to(WeatherDataStore.class);
            }
        };
    }

    private SensorSocketServer createSensorSocketServer(SensorMiddlewareConfiguration cfg, WeatherDataStore db) {
        NioEventLoopGroup acceptorGroup = new NioEventLoopGroup(
                cfg.getSocketServerAcceptorThreads()
        );

        NioEventLoopGroup handlerGroup = new NioEventLoopGroup(
                cfg.getSocketServerHandlerThreads()
        );

        return new SensorSocketServer(
                acceptorGroup,
                handlerGroup,
                cfg.getSocketServerPort(),
                new PersistingSensorReadingHandler(db)
        );
    }

    private WeatherDataStore createWeatherDataStore(SensorMiddlewareConfiguration cfg) {
        Connection connection = cfg.getDbConnection();
        DSLContext dsl = DSL.using(connection);
        return new WeatherDataStore(dsl, cfg.getDbName());
    }
}