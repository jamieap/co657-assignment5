package uk.ac.kent.co657.a5.sockets.readings.model;

import com.google.common.base.MoreObjects;

public class DHT22 {
    private final Float temperature;
    private final Float humidity;

    public DHT22(Float humidity, Float temperature) {
        this.humidity = humidity;
        this.temperature = temperature;
    }

    public Float getTemperature() {
        return temperature;
    }

    public Float getHumidity() {
        return humidity;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("temperature", temperature)
                .add("humidity", humidity)
                .toString();
    }
}
