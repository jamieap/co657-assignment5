package uk.ac.kent.co657.a5.openweather;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.api.client.repackaged.com.google.common.base.Throwables;
import com.google.common.util.concurrent.ListenableFuture;
import io.dropwizard.lifecycle.Managed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.kent.co657.a5.openweather.client.OpenWeatherClient;
import uk.ac.kent.co657.a5.openweather.model.SimpleWeatherData;
import uk.ac.kent.co657.a5.persistence.WeatherDataStore;
import uk.ac.kent.co657.a5.persistence.model.DataType;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This class periodically fetches data from openweathermap.org through the use of
 * {@link OpenWeatherClient} and stores it using {@link WeatherDataStore}
 */
public class OpenWeatherDataFetcher implements Runnable, Managed {

    private static final Logger LOG = LoggerFactory.getLogger(OpenWeatherDataFetcher.class);
    private static final String OPEN_WEATHER = "OpenWeather";

    private final ScheduledExecutorService executor = new ScheduledThreadPoolExecutor(1);
    private final WeatherDataStore db;
    private final OpenWeatherClient client;
    private final String location;

    private LocalDateTime lastFetch = null;

    public OpenWeatherDataFetcher(WeatherDataStore db, OpenWeatherClient client, String location) {
        this.db = checkNotNull(db);
        this.client = checkNotNull(client);
        this.location = checkNotNull(Strings.emptyToNull(location));
    }

    @Override
    public void start() {
        LOG.info("OpenWeather data fetcher started");
        executor.scheduleAtFixedRate(this, 0, 1, TimeUnit.HOURS);
    }

    @Override
    public void stop() {
        LOG.info("OpenWeather data fetcher stopped");
        executor.shutdown();
    }

    public LocalDateTime getLastFetchTime() {
        return lastFetch;
    }

    @Override
    public void run() {
        LOG.info("Fetching data from OpenWeather");
        try {
            SimpleWeatherData data = client.getWeatherData(location).get(10, TimeUnit.SECONDS);
            LocalDateTime now = LocalDateTime.now();
            db.saveData(
                    now,
                    DataType.HUMIDITY,
                    data.getHumidity().floatValue(),
                    OPEN_WEATHER
            );
            db.saveData(
                    now,
                    DataType.PRESSURE,
                    data.getPressure().floatValue(),
                    OPEN_WEATHER
            );
            db.saveData(
                    now,
                    DataType.TEMPERATURE,
                    data.getTemperature().floatValue(),
                    OPEN_WEATHER
            );
            lastFetch = now;
        } catch (Exception e) {
            LOG.error("Error received while fetching data", e);
        }
        LOG.info("Finished fetching data from OpenWeather");
    }
}