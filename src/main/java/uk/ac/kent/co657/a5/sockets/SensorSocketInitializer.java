package uk.ac.kent.co657.a5.sockets;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.kent.co657.a5.sockets.readings.model.SensorReading;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Performs the initial set up of sockets as they connect to Netty.
 * Registers the pipeline of handlers that messages received are passed through
 */
public class SensorSocketInitializer extends ChannelInitializer<SocketChannel> {

    private static final Logger LOG = LoggerFactory.getLogger(SensorSocketInitializer.class);
    private final PersistingSensorReadingHandler handler;

    public SensorSocketInitializer(PersistingSensorReadingHandler handler) {
        this.handler = checkNotNull(handler);
    }

    @Override
    public void initChannel(SocketChannel ch) throws Exception {
        LOG.info("Client connected: {}", ch.remoteAddress());

        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast(LineBasedFrameDecoder.class.getName(), new LineBasedFrameDecoder(256));
        pipeline.addLast(StringDecoder.class.getName(), new StringDecoder(CharsetUtil.UTF_8));
        pipeline.addLast(JsonDecoder.class.getName(), new JsonDecoder<>(SensorReading.class));
        pipeline.addLast(handler.getClass().getName(), handler);

        ch.closeFuture()
                .addListener(
                        f -> LOG.info(
                                "Client disconnected: {}",
                                ch.closeFuture().channel().remoteAddress()
                        )
                );
    }
}
