package uk.ac.kent.co657.a5.api;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimaps;
import uk.ac.kent.co657.a5.persistence.WeatherDataStore;
import uk.ac.kent.co657.a5.persistence.model.DataType;
import uk.ac.kent.co657.a5.persistence.Granularity;
import uk.ac.kent.co657.a5.persistence.model.WeatherRecord;

import javax.annotation.Resource;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * API endpoint for exposing weather data to clients using JSON
 */
@Path("/weather")
@Resource
public class WeatherDataResource {
    
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public ImmutableListMultimap<String, WeatherRecord> getWeather(
            @Context WeatherDataStore db,
            @QueryParam("granularity") String granularityStr,
            @QueryParam("buckets") Integer buckets,
            @QueryParam("type") String type
    ) {
        Granularity granularity = Granularity.valueOf(Granularity.class, granularityStr.toUpperCase());
        DataType dataType = DataType.valueOf(type.toUpperCase());
        List<WeatherRecord> data = db.getData(dataType, granularity, buckets);
        return Multimaps.index(data, WeatherRecord::getSource);
    }
}
