package uk.ac.kent.co657.a5.twitter;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import io.dropwizard.lifecycle.Managed;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import uk.ac.kent.co657.a5.persistence.Granularity;
import uk.ac.kent.co657.a5.persistence.WeatherDataStore;
import uk.ac.kent.co657.a5.persistence.model.DataType;
import uk.ac.kent.co657.a5.persistence.model.WeatherRecord;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This class uses the Twitter API to tweet the average values for weather data types over
 * the last 6 hour period
 */
public class DataTweeter implements Managed, Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(DataTweeter.class);
    private static final String INSIDE_MESSAGE_FORMAT = "The average %s inside over the past 6 hours was %.2f +/- %.2f";
    private static final String OUTSIDE_MESSAGE_FORMAT = "The average %s outside over the past 6 hours was %.2f +/- %.2f";
    private static final ImmutableList<String> INSIDE_SOURCES = ImmutableList.of("mbed-downstairs", "mbed-upstairs");
    private static final ImmutableList<String> OUTSIDE_SOURCES = ImmutableList.of("OpenWeather", "mbed-outside");

    private final ScheduledExecutorService executor = new ScheduledThreadPoolExecutor(1);

    private final Twitter twitter;
    private final WeatherDataStore db;

    public DataTweeter(Twitter twitter, WeatherDataStore db) {
        this.twitter = checkNotNull(twitter);
        this.db = checkNotNull(db);
    }

    public void tweet(String msg) {
        try {
            twitter.updateStatus(msg);
        } catch (TwitterException e) {
            throw Throwables.propagate(e);
        }
    }

    @Override
    public void start() throws Exception {
        LOG.info("DataTweeter started");
        executor.scheduleAtFixedRate(this, 0, 6, TimeUnit.HOURS);
    }

    @Override
    public void stop() throws Exception {
        LOG.info("DataTweeter stopped");
    }

    @Override
    public void run() {
        LOG.info("Tweeting data");
        tweetData(INSIDE_SOURCES, DataType.TEMPERATURE, INSIDE_MESSAGE_FORMAT);
        tweetData(INSIDE_SOURCES, DataType.HUMIDITY, INSIDE_MESSAGE_FORMAT);
        tweetData(INSIDE_SOURCES, DataType.PRESSURE, INSIDE_MESSAGE_FORMAT);

        tweetData(OUTSIDE_SOURCES, DataType.HUMIDITY, OUTSIDE_MESSAGE_FORMAT);
        tweetData(OUTSIDE_SOURCES, DataType.TEMPERATURE, OUTSIDE_MESSAGE_FORMAT);
        tweetData(OUTSIDE_SOURCES, DataType.PRESSURE, OUTSIDE_MESSAGE_FORMAT);
        LOG.info("Finished tweeting data");
    }

    private void tweetData(List<String> sources, DataType datatype, String msgFormat) {
        SummaryStatistics stats = getStats(datatype, sources);
        tweet(
                String.format(
                        msgFormat,
                        datatype.name().toLowerCase(),
                        stats.getMean(),
                        stats.getStandardDeviation()
                )
        );
    }

    private SummaryStatistics getStats(DataType dataType, List<String> sources) {
        SummaryStatistics stats = new SummaryStatistics();
        List<WeatherRecord> data = db.getDataForSources(dataType, Granularity.HOUR, 6, sources);
        data.forEach(d -> stats.addValue(d.getValue()));
        return stats;
    }
}