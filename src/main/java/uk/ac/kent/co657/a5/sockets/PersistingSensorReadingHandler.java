package uk.ac.kent.co657.a5.sockets;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.kent.co657.a5.persistence.WeatherDataStore;
import uk.ac.kent.co657.a5.persistence.model.DataType;
import uk.ac.kent.co657.a5.sockets.readings.model.SensorReading;

import java.time.LocalDateTime;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A message handler that persists {@link SensorReading}
 */
@ChannelHandler.Sharable
public class PersistingSensorReadingHandler extends ChannelInboundMessageHandlerAdapter<SensorReading> {

    private static final Logger LOG = LoggerFactory.getLogger(PersistingSensorReadingHandler.class);

    private final WeatherDataStore db;

    public PersistingSensorReadingHandler(WeatherDataStore db) {
        this.db = checkNotNull(db);
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, SensorReading msg) throws Exception {
        LOG.debug("Attempting to persist %s", msg.toString());
        LocalDateTime ts = LocalDateTime.now();
        db.saveData(ts, DataType.PRESSURE, msg.getBmp180().getPressure(), msg.getName());
        db.saveData(ts, DataType.HUMIDITY, msg.getDht22().getHumidity(), msg.getName());
        db.saveData(ts, DataType.TEMPERATURE, msg.getBmp180().getTemperature(), msg.getName());
        db.saveData(ts, DataType.TEMPERATURE, msg.getDht22().getTemperature(), msg.getName());
        LOG.debug("Successfully persisted %s", msg.toString());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        LOG.error("Exception while handling message", cause);
    }
}