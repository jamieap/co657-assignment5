package uk.ac.kent.co657.a5.persistence.model;

import com.google.common.base.MoreObjects;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class WeatherRecord {

    private final String source;
    private final DataType type;
    private final Float value;
    private final LocalDateTime ts;

    public WeatherRecord(String source, DataType type, Float value, LocalDateTime ts) {
        this.source = source;
        this.type = type;
        this.value = value;
        this.ts = ts;
    }

    public String getSource() {
        return source;
    }

    public DataType getType() {
        return type;
    }

    public Float getValue() {
        return value;
    }

    public String getTimestamp() {
        return ts.format(DateTimeFormatter.ISO_DATE_TIME);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("source", source)
                .add("type", type)
                .add("value", value)
                .add("ts", ts)
                .toString();
    }
}
