package uk.ac.kent.co657.a5.sockets;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A simple {@link MessageToMessageDecoder} that turns decodes Strings into objects of type {@link T}
 * through the magical powers of {@link Gson}
 *
 * @param <T> The type to decode received JSON into
 */
public class JsonDecoder<T> extends MessageToMessageDecoder<String, T> {

    private static final Gson GSON = new GsonBuilder().create();

    private final Class<T> clazz;

    public JsonDecoder(Class<T> clazz, Class<?>... acceptedMsgTypes) {
        super(acceptedMsgTypes);
        this.clazz = checkNotNull(clazz);
    }

    @Override
    public T decode(ChannelHandlerContext ctx, String msg) throws Exception {
        return GSON.fromJson(msg, clazz);
    }
}