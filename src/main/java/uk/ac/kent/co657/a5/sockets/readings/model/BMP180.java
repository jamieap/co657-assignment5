package uk.ac.kent.co657.a5.sockets.readings.model;

import com.google.common.base.MoreObjects;

public class BMP180 {
    private final Float temperature;
    private final Float pressure;

    public BMP180(Float pressure, Float temperature) {
        this.pressure = pressure;
        this.temperature = temperature;
    }

    public Float getTemperature() {
        return temperature;
    }

    public Float getPressure() {
        return pressure;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("temperature", temperature)
                .add("pressure", pressure)
                .toString();
    }
}
