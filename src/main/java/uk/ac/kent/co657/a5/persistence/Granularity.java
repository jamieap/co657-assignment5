package uk.ac.kent.co657.a5.persistence;

import org.jooq.Field;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import static org.jooq.impl.DSL.*;

public enum Granularity {

    MONTH {
        @Override
        public Field<Integer> extractTemporalValue(String field) {
            return field(String.format(EXTRACT_YEAR_MONTH_FORMAT, field), Integer.class);

        }

        @Override
        public Timestamp lowerBoundOfBuckets(Integer buckets) {
            LocalDateTime now = truncate(LocalDateTime.now());
            return Timestamp.from(
                    now.minus(buckets - 1, ChronoUnit.MONTHS)
                            .toInstant(ZoneOffset.UTC)
            );
        }

        @Override
        public LocalDateTime truncate(LocalDateTime dt) {
            return dt.minusSeconds(dt.getSecond())
                    .minusMinutes(dt.getMinute())
                    .minusHours(dt.getHour())
                    .minusDays(dt.getDayOfMonth() - 1);
        }
    },
    DAY {
        @Override
        public Field<Integer> extractTemporalValue(String field) {
            return field(String.format(EXTRACT_MONTH_FORMAT, field), Integer.class)
                    .concat(field(String.format(EXTRACT_DAY_FORMAT, field), Integer.class))
                    .cast(Integer.class);
        }

        @Override
        public Timestamp lowerBoundOfBuckets(Integer buckets) {
            LocalDateTime now = truncate(LocalDateTime.now());
            return Timestamp.from(
                    now.minus(buckets - 1, ChronoUnit.DAYS)
                            .toInstant(ZoneOffset.UTC)
            );
        }

        @Override
        public LocalDateTime truncate(LocalDateTime dateTime) {
            return dateTime.truncatedTo(ChronoUnit.DAYS);
        }
    },
    HOUR {
        @Override
        public Field<Integer> extractTemporalValue(String field) {
            return field(String.format(EXTRACT_DAY_HOUR_FORMAT, field), Integer.class);
        }

        @Override
        public Timestamp lowerBoundOfBuckets(Integer buckets) {
            LocalDateTime now = truncate(LocalDateTime.now());
            return Timestamp.from(
                    now.minus(buckets - 1, ChronoUnit.HOURS)
                            .toInstant(ZoneOffset.UTC)
            );
        }

        @Override
        public LocalDateTime truncate(LocalDateTime dateTime) {
            return dateTime.truncatedTo(ChronoUnit.HOURS);
        }
    },
    MINUTE {
        @Override
            public Field<Integer> extractTemporalValue(String field) {
            return field(String.format(EXTRACT_DAY_MINUTE_FORMAT, field), Integer.class)
                    .cast(Integer.class);
        }

        @Override
        public Timestamp lowerBoundOfBuckets(Integer buckets) {
            LocalDateTime now = truncate(LocalDateTime.now());
            return Timestamp.from(
                    now.minus(buckets - 1, ChronoUnit.MINUTES)
                            .toInstant(ZoneOffset.UTC)
            );
        }

        @Override
        public LocalDateTime truncate(LocalDateTime dateTime) {
            return dateTime.truncatedTo(ChronoUnit.MINUTES);
        }
    };

    private static final String EXTRACT_YEAR_MONTH_FORMAT = "EXTRACT(YEAR_MONTH FROM %s)";
    private static final String EXTRACT_MONTH_FORMAT = "EXTRACT(MONTH FROM %s)";
    private static final String EXTRACT_DAY_FORMAT = "EXTRACT(DAY FROM %s)";
    private static final String EXTRACT_DAY_HOUR_FORMAT = "EXTRACT(DAY_HOUR FROM %s)";
    private static final String EXTRACT_DAY_MINUTE_FORMAT = "EXTRACT(DAY_MINUTE FROM %s)";

    public abstract Field<Integer> extractTemporalValue(String field);

    /**
     * For this Granularity and a number of buckets, compute the lower bound date of the last
     * bucket relative to now.
     */
    public abstract Timestamp lowerBoundOfBuckets(Integer buckets);

    /**
     * Truncates a {@link LocalDateTime} to the least descriptive precision required for this
     * Granularity
     */
    public abstract LocalDateTime truncate(LocalDateTime dateTime);
}