package uk.ac.kent.co657.a5.openweather.model;

import uk.ac.kent.co657.a5.openweather.model.autogenerated.WeatherData;

public class SimpleWeatherData {

    private final WeatherData weatherData;

    public SimpleWeatherData(WeatherData weatherData) {
        this.weatherData = weatherData;
    }

    public Double getTemperature() {
        return weatherData.getMain().getTemp();
    }

    public Double getPressure() {
        return weatherData.getMain().getPressure();
    }

    public Double getHumidity() {
        return weatherData.getMain().getHumidity();
    }
}
