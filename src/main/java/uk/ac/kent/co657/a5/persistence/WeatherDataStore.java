package uk.ac.kent.co657.a5.persistence;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.jooq.*;
import uk.ac.kent.co657.a5.persistence.model.DataType;
import uk.ac.kent.co657.a5.persistence.model.WeatherRecord;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.jooq.impl.DSL.*;

/**
 * An interface to an SQL database for storing and retrieving weather data
 * <p>
 * MySQL schema:
 * <p>
 * CREATE TABLE `weather_data` (
 * `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 * `type` varchar(32) NOT NULL DEFAULT '',
 * `value` float NOT NULL,
 * `timestamp` datetime NOT NULL,
 * `source` varchar(32) NOT NULL DEFAULT '',
 * PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */
public class WeatherDataStore {

    private final DSLContext db;
    private final Table<?> table;

    public WeatherDataStore(DSLContext db, String tableName) {
        this.db = checkNotNull(db);
        this.table = table(checkNotNull(Strings.emptyToNull(tableName)));
    }

    public void saveData(LocalDateTime time, DataType type, Float value, String source) {
        db.insertInto(table)
                .set(toRecord(time, type, value, source))
                .execute();
    }

    public Map<String, LocalDateTime> getSourceInfo() {
        Map<String, LocalDateTime> collect =
                db.select(field("source", String.class), max(field("timestamp", Timestamp.class)))
                        .from(table)
                        .groupBy(field("source"))
                        .orderBy(max(field("timestamp")).desc())
                        .fetch()
                        .stream()
                        .collect(
                                Collectors.toMap(
                                        Record2::value1,
                                        rec -> rec.value2().toLocalDateTime())
                        );

        return ImmutableMap.copyOf(collect);
    }

    public List<WeatherRecord> getDataForSources(
            DataType type,
            Granularity granularity,
            Integer buckets,
            List<String> sources
    ) {
        SelectSelectStep<Record5<BigDecimal, Integer, String, Timestamp, String>> select = db.select(
                avg(field("value", Float.class)).as("value"),
                granularity.extractTemporalValue("timestamp").as("temporal"),
                field("source", String.class),
                min(field("timestamp", Timestamp.class)),
                field("type", String.class)
        );

        SelectConditionStep<Record5<BigDecimal, Integer, String, Timestamp, String>> where =
                sources == null || sources.isEmpty() ?
                        select.from(table).where(field("type").eq(type.toString())) :
                        select.from(table).where(field("type").eq(type.toString())
                                .and(field("source").in(sources)));

        Result<Record5<BigDecimal, Integer, String, Timestamp, String>> records =
                where.and(field("timestamp", Timestamp.class).greaterOrEqual(granularity.lowerBoundOfBuckets(buckets)))
                        .groupBy(field("source"), granularity.extractTemporalValue("timestamp"))
                        .orderBy(min(field("timestamp", Timestamp.class)).desc())
                        .fetch();

        return records.stream()
                .map(r -> fromRecord(r, granularity))
                .collect(Collectors.toList());
    }

    public List<WeatherRecord> getData(DataType type, Granularity granularity, Integer buckets) {
        return getDataForSources(type, granularity, buckets, ImmutableList.of());
    }

    private WeatherRecord fromRecord(
            Record5<BigDecimal, Integer, String, Timestamp, String> r,
            Granularity granularity
    ) {
        return new WeatherRecord(
                r.value3(),
                DataType.valueOf(r.value5()),
                r.value1().floatValue(),
                granularity.truncate(r.value4().toLocalDateTime())
        );
    }

    private ImmutableMap<Field<?>, ?> toRecord(
            LocalDateTime ts,
            DataType type,
            Float value,
            String source
    ) {
        return ImmutableMap.of(
                field("type"), type.toString(),
                field("value"), value,
                field("source"), source,
                field("timestamp"), Date.from(ts.toInstant(ZoneOffset.UTC))
        );
    }
}